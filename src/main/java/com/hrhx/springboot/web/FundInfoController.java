package com.hrhx.springboot.web;

import com.hrhx.springboot.domain.FundInfo;
import com.hrhx.springboot.domain.FundPublic;
import com.hrhx.springboot.mysql.repository.FundInfoRepository;
import com.hrhx.springboot.mysql.repository.FundPublicRepository;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
 * 
 * @author duhongming
 *
 */
@Controller
@RequestMapping(value="/fund")
public class FundInfoController {
	
	@Autowired
	private FundInfoRepository fundInfoRepository;
	
	@Autowired
	private FundPublicRepository fundPublicRepository;
	
	private final static String  FUND_PUBLIC_URL = "http://fund.eastmoney.com/data/rankhandler.aspx?op=ph&dt=kf&ft=all&rs=&gs=0&sc=zzf&st=desc&sd=2016-09-04&ed=2017-09-04&qdii=&tabSubtype=,,,,,&pi=1&pn=10000&dx=1&v=0.8597183667182815";
	@RequestMapping(value="updateFundPublic")
	public void updateFundPublic(){
		Document doc = null;
		try {
			//获取所有基金数据
			doc = Jsoup.connect(FUND_PUBLIC_URL).get();
			//过滤脏数据
			String json = doc.text().replace("var rankData = ", "").replace(";", "");
//			//解析数据
//			JsonObject jsonObject= JsonUtil.parseJson(json);
//			//获取数据
//			JsonArray datas= jsonObject.get("datas").getAsJsonArray();
//			//遍历数据
//			for(int i=0;i<datas.size();i++){
//				String data = datas.get(i).toString().replace("\"", "");
//				String fundCode = data.split(",")[0];
//				String fundName = data.split(",")[1];
//				FundPublic fundPublic = new FundPublic(fundCode,fundName);
//				fundPublicRepository.save(fundPublic);
//			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}	
	}
	
	@RequestMapping(value="updateFundData")
	public void updateData() throws IOException, InterruptedException{
		List<FundPublic> fundPublicList = fundPublicRepository.findAll();
		for(FundPublic fundPublic: fundPublicList){
			Thread.sleep(2000);
			new FundService(fundPublic.getFundCode(),fundPublic.getFundName()).start();
//			updateFund(fundPublic.getFundCode(),fundPublic.getFundName());
		}
	}	
	
	public class FundService extends Thread{
		private final static String  FUND_DATA_URL = "http://fund.eastmoney.com/f10/F10DataApi.aspx?type=lsjz&code=${fundCode}&page=1&per=20000&sdate=&edate=&rt=0.07694074122676131";
		private String fundCode;
		private String fundName;
//		public void updateFund(String fundCode,String fundName){
		/**
		 * 通过构造方法给线程名字赋值
		 * @param fundCode
		 * @param fundName
		 */
	    public FundService(String fundCode,String fundName) {
	    	// 给线程名字赋值
	    	super(fundCode);
	    	this.fundCode = fundCode;
	    	this.fundName = fundName;
	    }
		@Override
		public void run() {
			Document doc = null;
			try {
				doc = Jsoup.connect(FUND_DATA_URL.replace("${fundCode}", fundCode)).get();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			Elements trs = doc.select("table").select("tbody").select("tr");
			List<FundInfo> fundInfoList= new ArrayList<FundInfo>();
			for(int i=0;i<trs.size();i++){
				try{
				Element element = trs.get(i);
				Elements tds = element.select("td");

				String netValueDate = tds.get(0).html().substring(0, 10);
				Double everyThanAccrual = Double.parseDouble(tds.get(1).html());
				Double annualizedYield7 = Double.parseDouble(tds.get(2).html().replace("%", ""));

				FundInfo fundInfo = new FundInfo(fundCode,fundName,everyThanAccrual,annualizedYield7,netValueDate);
				fundInfoList.add(fundInfo);
				}catch(Exception e){
					continue;
				}
			}
			fundInfoRepository.save(fundInfoList);
			System.out.println(getName() + "批量保存成功");
		}
		public String getFundCode() {
			return fundCode;
		}
		public void setFundCode(String fundCode) {
			this.fundCode = fundCode;
		}
		public String getFundName() {
			return fundName;
		}
		public void setFundName(String fundName) {
			this.fundName = fundName;
		}

	}
}

