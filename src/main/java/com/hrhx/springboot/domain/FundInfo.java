package com.hrhx.springboot.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
/**
 * 
 * @author duhongming
 *
 */
@Entity
public class FundInfo {
	
	@Id
    @GeneratedValue
	private Integer id;
	
	@Column(nullable = false)
	private String fundCode;
	
	@Column(nullable = false)
	private String fundName;

	@Column(nullable = false)
	private Double everyThanAccrual;
	
	@Column(nullable = false)
	private Double annualizedYield7;
	
	@Column(nullable = false)
	private String netvalueDate;

	public FundInfo() {
		super();
	}

	public FundInfo(String fundCode, String fundName, Double everyThanAccrual, Double annualizedYield7,
			String netvalueDate) {
		super();
		this.fundCode = fundCode;
		this.fundName = fundName;
		this.everyThanAccrual = everyThanAccrual;
		this.annualizedYield7 = annualizedYield7;
		this.netvalueDate = netvalueDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFundCode() {
		return fundCode;
	}

	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public Double getEveryThanAccrual() {
		return everyThanAccrual;
	}

	public void setEveryThanAccrual(Double everyThanAccrual) {
		this.everyThanAccrual = everyThanAccrual;
	}

	public Double getAnnualizedYield7() {
		return annualizedYield7;
	}

	public void setAnnualizedYield7(Double annualizedYield7) {
		this.annualizedYield7 = annualizedYield7;
	}

	public String getNetvalueDate() {
		return netvalueDate;
	}

	public void setNetvalueDate(String netvalueDate) {
		this.netvalueDate = netvalueDate;
	}
	
}
