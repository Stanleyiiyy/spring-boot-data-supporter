package com.hrhx.springboot.mysql.service;

import java.util.List;

import com.hrhx.springboot.domain.Cnarea;
/**
 * 
 * @author duhongming
 *
 */
public interface CnareaService {
	/**
	 * 获取省级信息
	 * @return
	 * @throws Exception
	 */
	List<Cnarea> getAllFirstLevel() throws Exception;
	/**
	 * 根据父级别获取相应区域级别信息
	 * @param id
	 * @param level
	 * @return
	 * @throws Exception
	 */
	List<Cnarea> getByParentId(Integer id,Integer level) throws Exception;
}
