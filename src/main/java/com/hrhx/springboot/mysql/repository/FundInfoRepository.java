package com.hrhx.springboot.mysql.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hrhx.springboot.domain.FundInfo;
/**
 * 
 * @author duhongming
 *
 */
public interface FundInfoRepository extends JpaRepository<FundInfo, Long>{

}
