# spring-boot-data-supporter

本项目的Mysql和MongoDB数据库（下面是详细的处理过程，免做伸手党，学习学习没坏处）：

https://git.oschina.net/duhongming/spring-boot-data-supporter/attach_files

## [1 地理区域5级联动](#1-地理区域5级联动-1)
## [2 身份证信息](#2-身份证信息-1)
## [3 全国天气情况](#3-全国天气情况-1)
## [4 基金决策分析](#4-基金决策分析-1)
## [5 ip地址定位库](#5-ip地址定位库-1)
## [6 一元线性回归获取样本数据建立模型](#6-一元线性回归获取样本数据建立模型-1)
## [7 Ansj中文分词服务](#7-ansj中文分词服务-1)

## 1 地理区域5级联动

### 1.1 数据源导入到Mysql和MongoDB中
-------------------------------------------------------------------------------

1) 下载中国5级行政区域mysql库：https://github.com/kakuilan/china_area_mysql

2) 通过MySQL Workbench 6.2 CE打开>>>Data Import/Restore>>>Import from Self-Contained File>>>Default Target Schema>>>Start Import
```
#数据库设置问题：set global max_allowed_packet=1024*1024*160这样就好了（因为太大了100多M，所以我设置了160M）：

09:15:03 Restoring C:\Users\Administrator\Desktop\cnarea20160731.sql
Running: mysql.exe --defaults-file="c:\users\admini~1\appdata\local\temp\tmp3aq7h3.cnf"  --host=localhost --user=root --port=3306 --default-character-set=utf8 --comments --database=data-supporter < "C:\\Users\\Administrator\\Desktop\\cnarea20160731.sql"
ERROR 2006 (HY000) at line 22: MySQL server has gone away
Operation failed with exitcode 1
09:15:04 Import of C:\Users\Administrator\Desktop\cnarea20160731.sql has finished with 1 errors
```

3） 重命名表：cnarea

--------------------------------------------------------------------------------

4）Mysql转MongoDB数据库用的是MongoVUE，Collections右键>>>RDBMS Import>>>from Mysql


5）更新数据库中集合的字段
```
db.cnarea.update({}, {$rename : {"parent_id" : "parentId","area_code" : "areaCode","zip_code" : "zipCode","city_code" : "cityCode","short_name" : "shortName","merger_name" : "mergerName"}}, false, true);
```


6）windows导出MongoDB数据库
```
mongodump -d data-supporter -c cnarea -o D:/mongodump/
```

7）windows导入MongoDB数据库(支持跨服务器，非常方便，速度非常快）
```
mongorestore -h 192.168.1.108 -d data-supporter --dir D:/mongodump/data-supporter/
```
--------------------------------------------------------------------------------

### 1.2 修改数据库配置信息
application.properties配置文件中
```
spring.datasource.url=jdbc:mysql://127.0.0.1:3306/data-supporter
spring.datasource.username=root
spring.datasource.password=root
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.data.mongodb.uri=mongodb://127.0.0.1:27017/data-supporter
```
### 1.3 请通过swagger进行项目调试

http://localhost:8080/swagger-ui.html


地理区域省级调试页面：
![alt text](./src/main/resources/static/demo/province.png "地理区域省级调试页面")

地理区域第2-5级调试页面：
![alt text](./src/main/resources/static/demo/level.png "地理区域第2-5级调试页面")

### 1.4 地理区域5级联动演示页面

http://localhost:8080/

![alt text](./src/main/resources/static/demo/cnarea.png "地理区域5级联动")


## 2 身份证信息

### 2.1 下载身份证信息json库

https://github.com/NoBey/IdCard

下载IdCard/data/data.json，重命名为idcard.json，放到resources\static文件夹下

### 2.2 身份证信息用Gson将Json数据转成Map，其他的Json框架都试了，都不好用
```
Map<String, Object> map = JsonUtil.toMap(JsonUtil.parseJson(injectInfo()));
```

### 2.3 idcard身份证信息获取出生年月日、岁数、性别、生肖、星座、地理位置
![alt text](./src/main/resources/static/demo/idcard.png "idcard身份证")
![alt text](./src/main/resources/static/demo/idcardinfo.png "idcard身份证信息")



## 3 全国天气情况

### 3.1 全国天气情况

http://www.weather.com.cn/

### 3.2 获取某个城市的一段时间的天气
![alt text](./src/main/resources/static/demo/cnweather.png "天气详细信息测试")
![alt text](./src/main/resources/static/demo/cnweather_result.png "天气详细信息结果")

## 4 基金决策分析

程序优化（比以前快上百倍）：

1.捕获异常数据，继续

2.加入多线程

3.批量保存

### 4.1 所有基金代码和基金名称获取
http://fund.eastmoney.com/data/rankhandler.aspx?op=ph&dt=kf&ft=all&rs=&gs=0&sc=zzf&st=desc&sd=2016-09-04&ed=2017-09-04&qdii=&tabSubtype=,,,,,&pi=1&pn=10000&dx=1&v=0.8597183667182815

### 4.2 所有基金每万份收益、7日年化收益率（%）
 http://fund.eastmoney.com/f10/F10DataApi.aspx?type=lsjz&code=${fundCode}&page=1&per=20000&sdate=&edate=&rt=0.07694074122676131

### 4.3 分析语句
```
SELECT
	`fund_info`.`FUND_CODE` AS `基金代码`,
	`fund_info`.`FUND_NAME` AS `基金货币`,
	round(
		avg(
			`fund_info`.`EVERY_THAN_ACCRUAL`
		),
		2
	) AS `每万份收益`,
	round(
		avg(
			`fund_info`.`ANNUALIZED_YIELD7`
		),
		2
	) AS `7日年化收益率（%）`
FROM
	`fund_info`
WHERE
	(
		(
			`fund_info`.`NETVALUE_DATE` >= '2017-01-01'
		)
		AND (
			`fund_info`.`NETVALUE_DATE` LIKE '%-%'
		)
	)
GROUP BY
	`fund_info`.`FUND_NAME`
ORDER BY
	round(
		avg(
			`fund_info`.`EVERY_THAN_ACCRUAL`
		),
		2
	) DESC
```

## 5 ip地址定位库

1) 下载ip地址定位库：https://git.oschina.net/lionsoul/ip2region

2) 在application.properties配置关联路径即可：

```
ip.region.db.file=D:\\gitRespo\\open-source\\spring-boot-data-supporter\\resources\\static\\ipdata\\ip2region.db
```
3） 演示地址：http://localhost:8080/swagger-ui.html

![alt text](./src/main/resources/static/demo/ip2region.png "ip地址定位")


## 6 一元线性回归获取样本数据建立模型
![alt text](./src/main/resources/static/demo/monadicline.png "一元线性回归获取样本数据建立模型")

## 7 Ansj中文分词服务
https://github.com/NLPchina/ansj_seg

**Spring Boot 教程大家学习下：**
http://blog.didispace.com/categories/Spring-Boot/page/4/
